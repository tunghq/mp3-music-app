package vn.hqt.tunghq.appnhac.Fragment;

import static android.app.PendingIntent.getActivities;
import static android.app.PendingIntent.getActivity;
import static android.content.Intent.getIntent;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import vn.hqt.tunghq.appnhac.Activity.DanhSachBaiHatActivity;
import vn.hqt.tunghq.appnhac.Activity.DanhSachPlaylistActivity;
import vn.hqt.tunghq.appnhac.Activity.MainActivity;
import vn.hqt.tunghq.appnhac.Activity.ui.login.LoginActivity;
import vn.hqt.tunghq.appnhac.Adapter.DanhSachBaiHatAdapter;
import vn.hqt.tunghq.appnhac.Adapter.DanhSachPlaylistAdapter;
import vn.hqt.tunghq.appnhac.Adapter.PlayListAdapter;
import vn.hqt.tunghq.appnhac.Model.Album;
import vn.hqt.tunghq.appnhac.Model.BaiHat;
import vn.hqt.tunghq.appnhac.Model.PlayList;
import vn.hqt.tunghq.appnhac.Model.QuangCao;
import vn.hqt.tunghq.appnhac.Model.TheLoai;
import vn.hqt.tunghq.appnhac.R;
import vn.hqt.tunghq.appnhac.Service.APIService;
import vn.hqt.tunghq.appnhac.Service.DataService;


import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;



public class AccountFragment extends Fragment {
    CoordinatorLayout coordinatorLayout;
    ListView lvPlayList;

    QuangCao quangCao;
    ImageView imgDanhSachCaKhuc;
    ArrayList<BaiHat> mangBaiHat;
    DanhSachBaiHatAdapter danhSachBaiHatAdapter;
    ArrayList<PlayList> arrayListPlayList1;
    TextView tvlogout,tvuser;
    View view;
    TextView tvMyPlaylist;
    TextView txtTitlePlayList,txtViewXemThemPlayList;
    PlayListAdapter playListAdapter;
    RecyclerView recyclerViewDanhSachPlaylist;
    private LoginActivity loginActivity;
    // final SharedPreferences sharedPreferences = getSharedPreferences("data", Context.MODE_PRIVATE);


    public AccountFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters




    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.fragment_account, container, false);
        anhxa();
        return view;
    }


    private void anhxa() {
        tvuser= view.findViewById(R.id.tvUser);

        tvMyPlaylist= view.findViewById(R.id.tvMyPlaylist);
        //updateMyPlaylist();
        tvMyPlaylist.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), DanhSachPlaylistActivity.class);

                startActivity(intent);
            }
        });
        tvlogout = view.findViewById(R.id.textViewLogout);
        tvlogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //SharedPreferences.Editor editor=sharedPreferences.edit();
                //editor.clear();
                //editor.commit();

                Intent intent = new Intent(getActivity(), LoginActivity.class);
                startActivity(intent);
            }
        });
    }
    public void getData() {
        DataService dataService = APIService.getService();
        Call<List<PlayList>> callBack = dataService.getDanhSachmyPlaylist();
        callBack.enqueue(new Callback<List<PlayList>>() {
            @Override
            public void onResponse(Call<List<PlayList>> call, Response<List<PlayList>> response) {
                arrayListPlayList1 = (ArrayList<PlayList>) response.body();
                playListAdapter = new PlayListAdapter(getActivity(), android.R.layout.simple_list_item_1, arrayListPlayList1);
                lvPlayList.setAdapter(playListAdapter);
                setListViewHeightBasedOnChildren(lvPlayList);
                lvPlayList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Intent intent = new Intent(getActivity(), DanhSachBaiHatActivity.class);
                        intent.putExtra("itemmyplaylist", arrayListPlayList1.get(position));
                        startActivity(intent);
                    }

                });
            }

            @Override
            public void onFailure(Call<List<PlayList>> call, Throwable t) {

            }
        });

    }
    public void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = listView.getPaddingTop() + listView.getPaddingBottom();
        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.AT_MOST);
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);

            if(listItem != null){
                // This next line is needed before you call measure or else you won't get measured height at all. The listitem needs to be drawn first to know the height.
                listItem.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
                listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
                totalHeight += listItem.getMeasuredHeight();

            }
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }
}
