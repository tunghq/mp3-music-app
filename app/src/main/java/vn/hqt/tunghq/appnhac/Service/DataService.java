package vn.hqt.tunghq.appnhac.Service;

import vn.hqt.tunghq.appnhac.Model.Album;
import vn.hqt.tunghq.appnhac.Model.BaiHat;
import vn.hqt.tunghq.appnhac.Model.ChuDe;
import vn.hqt.tunghq.appnhac.Model.PlayList;
import vn.hqt.tunghq.appnhac.Model.QuangCao;
import vn.hqt.tunghq.appnhac.Model.TheLoai;
import vn.hqt.tunghq.appnhac.Model.TheLoaiTrongNgay;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface DataService {

    @GET("songbanner.php")
    Call<List<QuangCao>> getDataBanner();

    @GET("playlistforcurrentday.php")
    Call<List<PlayList>> getPlayListCurrentDay();

    @GET("Chudevatheloai.php")
    Call<TheLoaiTrongNgay> getCategoryMusic();

    @GET("Albumhot.php")
    Call<List<Album>> getAlbumHot();

    @GET("FavoriteSong.php")
    Call<List<BaiHat>> getBaiHatHot();

    @FormUrlEncoded
    @POST("Danhsachbaihat.php")
    Call<List<BaiHat>> getDanhSachBaiHatTheoQuangCao(@Field("idquangcao") String idQuangCao);

    @FormUrlEncoded
    @POST("Danhsachbaihat.php")
    Call<List<BaiHat>> getDanhSachBaiHatTheoPlayList(@Field("idplaylist") String idPlayList);


    @GET("danhsachcacplaylist.php")
    Call<List<PlayList>> getDanhSachPlaylist();

    @GET("loadmyplaylist.php")
    Call<List<PlayList>> getDanhSachmyPlaylist();

    @FormUrlEncoded
    @POST("danhsachbaihatChudetheloai.php")
    Call<List<BaiHat>> getDanhSachBaiHatTheoTheLoai(@Field("idtheloai") String idTheLoai);

    @GET("tatcachude.php")
    Call<List<ChuDe>> getAllChuDe();

    @FormUrlEncoded
    @POST("theloaitheochude.php")
    Call<List<TheLoai>> getTheLoaiTheoChuDe(@Field("idchude") String idChuDe);

    @GET("tatcaalbum.php")
    Call<List<Album>> getAlbum();

    @FormUrlEncoded
    @POST("danhsachbaihatAlbum.php")
    Call<List<BaiHat>> getBaiHatTheoAlbum(@Field("idalbum") String idAlbum);

    @FormUrlEncoded
    @POST("capnhatluotthich.php")
    Call<String> updateLuotThich(@Field("luotthich") String luotthich, @Field("idbaihat") String idbaihat);

    @FormUrlEncoded
    @POST("searchbaihat.php")
    Call<List<BaiHat>> getSearchBaiHat(@Field("tukhoa") String tuKhoa);


    @FormUrlEncoded
    @POST("ThemPlaylist.php")
    Call<String> updatemyplaylist( @Field("idbaihat") String idbaihat);


}
