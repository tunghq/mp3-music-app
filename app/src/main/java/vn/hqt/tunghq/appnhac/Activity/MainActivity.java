package vn.hqt.tunghq.appnhac.Activity;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import vn.hqt.tunghq.appnhac.Adapter.MainViewPagerAdapter;
import vn.hqt.tunghq.appnhac.Fragment.FragmentTaiKhoan;
import vn.hqt.tunghq.appnhac.Fragment.FragmentTimKiem;
import vn.hqt.tunghq.appnhac.Fragment.FragmentTrangChu;
import vn.hqt.tunghq.appnhac.R;

public class MainActivity extends AppCompatActivity {

    TabLayout tabLayout;
    ViewPager viewPager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        anhXa();
        init();

    }

    public void init() {
        MainViewPagerAdapter mainViewPagerAdapter = new MainViewPagerAdapter(getSupportFragmentManager());
        mainViewPagerAdapter.addFragment(new FragmentTrangChu(), "Trang Chủ");
        mainViewPagerAdapter.addFragment(new FragmentTimKiem(), "Tìm Kiếm");
        mainViewPagerAdapter.addFragment(new FragmentTaiKhoan(), "Tài khoản");
        viewPager.setAdapter(mainViewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.getTabAt(0).setIcon(R.drawable.home3);
        tabLayout.getTabAt(1).setIcon(R.drawable.search2);
        tabLayout.getTabAt(2).setIcon(R.drawable.iconacc);
    }

    public void anhXa(){
        tabLayout = findViewById(R.id.myTabLayout);
        viewPager = findViewById(R.id.myViewPager);
    }
}
